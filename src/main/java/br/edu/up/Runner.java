package br.edu.up;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Runner implements CommandLineRunner {

    @Autowired
    private FilaDePedidosSender sender;

    @Override
    public void run(String... args) throws Exception {
        sender.send("Teste de envio do pedido por fila no RabbitMQ!");
        System.out.println("Mensagem enviada...");
    }
}