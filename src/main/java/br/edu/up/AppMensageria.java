package br.edu.up;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * Passos para uso do RabbitMQ:
 * 1. Instalar o compilador da linguagem ErLang: https://www.erlang.org/downloads
 * 2. Instalar o servidor RabbitMQ Server: https://www.rabbitmq.com/
 * 3. Reiniciar o computador
 * 4. Rodar no prompt de commando: rabbitmq-plugins enable rabbitmq_management
 * 5. Verificar se o servidor está rodando em: http://localhost:15672/#/queues
 * 6. Criar os produtores e consumidores
 * 7. Testar o envio e recebimento de mensagens 
 * 
 * @author Geucimar Brilhador
 *
 */

@EnableRabbit
@SpringBootApplication
public class AppMensageria {
	
	@Value("${fila.pedidos.nome}")
    private String nomeDaFila;

	public static void main(String[] args) {
		SpringApplication.run(AppMensageria.class, args);
	}
	
	@Bean
    public Queue fila() {
        return new Queue(nomeDaFila, true);
    }
}